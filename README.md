# Hosts-AdBlock-SiteBlocker

# What is this?

The following Bash script adds host files to Unix systems that block a variety of sites such as ad agencies, browser hijacking sites, disturbing sites (such as shock sites) and much more.

### How can I contact the developer of this script?

You can reach me at the following:
```
satanism@riseup.net
twitter.com/proanarchism
github.com/9-5
gitlab.com/American
```
## Credits

The guys behind [SomeoneWhoCares](https://someonewhocares.org).
This project can be freely modified. I would appreciate accrediation if possible <3.
