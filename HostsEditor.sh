#!/bin/bash
## This a simple Bash script that adds hosts to your Unix host files.
## These hosts block ads as well as various sites such as shock sites.
## Credit to the guys behind someonewhocares.org for the hosts.
## This script was written by Envy.
## satanism@riseup.net
## twitter.com/proanarchism
## github.com/9-5
## keybase.io/38
## This project can be found at https://github.com/9-5/Hosts-AdBlock-SiteBlocker/

sudo_chk ( ) {
    script="Hosts Modifier"
    if [ "$(id -nu)" != "root" ]; then
        sudo -k
        pass=$(whiptail --backtitle "$script Installer" --title "Authentication required." --passwordbox "Installing $script requires sudo access. Please authenticate to begin the installation.\n\n[sudo] Password for user $USER:" 12 50 3>&2 2>&1 1>&3-)
        exec sudo -S -p '' "$0" "$@" <<< "$pass"
        exit 1
    fi
}

dl_host ( ) {
    tmp_d=$(mktemp -d /tmp/tmp.XXXXXX)
    wget -P $tmp_d http://someonewhocares.org/hosts/hosts
    sudo cat $tmp_d/hosts >> /etc/hosts
    echo "Finished downloading hosts."
    echo "Adding hosts..."
    echo "Finished adding hosts."
    sudo rm -rf $tmp_d
}

dir_chk ( ) {
    dir="./HostBackups"
    if [ ! -d "$dir" ]
        then
            mkdir $dir
        else
            bk_host
    fi
}
           
bk_host ( ) {
    bk_f=$(touch ./HostBackups/"$(date +"%Y_%m_%d")_host.bak")
    cp /etc/hosts ./HostBackups/$bk_f
}

repeat ( ) {
    while true; do
        read -p "Would you like to back up your hosts file? (y/N]: " ab
        case $ab in
            [Yy]* ) dir_test; break;;
            [Nn]* ) echo "Press any key to exit the script."; read junk; exit;;
            * ) echo "Please only answer with a yes or no."; repeat;;
        esac
    done
}

sudo_chk
echo "Downloading host file..."
dl_host
while true; do
    read -p "Would you like to back up your hosts file? [y/N]: " ab
    case $ab in
        [Yy]* ) dir_chk; break;;
        [Nn]* ) echo "Press any key to exit the script."; read junk; exit;;
        * ) echo "Please only answer with a yes or no."; repeat;;
    esac
done
echo "Press any key to exit the script."
read junk
